<article class="container ebook obrigado">
    <div class="row">
        <div class="col">
            <h1>Muito obrigado pelo seu interesse no nosso e-book, agora basta realizar o download no botão abaixo!</h1>

            <p class="tagline">Barbeiro, você está no caminho certo para dar um UPGrade na qualidade do seu atendimento.
            </p>

            <a href="<?= BASE ?>/7-sinais-de-que-sua-barbearia-esta-no-caminho-certo.pdf" title="Faça o download do seu e-book" target="_blank">Baixe seu e-book aqui!</a>
        </div>       
    </div>
</article>