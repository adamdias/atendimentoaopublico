<?php
$Read = new Read;
require '_cdn/widgets/eadpagseguro/cart.inc.php';
require '_cdn/widgets/contact/contact.wc.php';
$semborda = null;
$carrinho = (!empty($_SESSION['wc_order']) ? str_pad(count($_SESSION['wc_order']), 2, 0, 0) : '00');
$menu = '
<li><a href="' . BASE . '" title="Início - ' . SITE_NAME . '">Início</a></li>
<li><a href="' . BASE . '/cursos" title="Cursos - ' . SITE_NAME . '">Cursos</a></li>
<li class="header_logo_menu"><a href="' .  BASE . '" title="Logotipo ' .  SITE_NAME  . '">
	<img width="170" title="Logotipo ' .  SITE_NAME . '" alt="[Logotipo ' .  SITE_NAME . ']" src="' .  INCLUDE_PATH  . '/images/logotipo.png" />	
</a></li>
<li><a href="' . BASE . '/contato" title="Contato - ' . SITE_NAME . '">Contato</a></li>
<li><a href="' . BASE . '/campus" title="Entrar/Cadastrar - ' . SITE_NAME . '">Entrar/Cadastrar</a></li>
<li class="header_carrinho"><a href="' . BASE . '/pedido" title="Carrinho de Compras - ' . SITE_NAME . '"><img width="20" src="' . INCLUDE_PATH . '/images/icon_cart.png" alt="Carrinho de Compras" title="Carrinho de Compras"/><span>' . $carrinho . '</span></a></li>
<li class="header_carrinho_mobile"><a href="' . BASE . '/pedido" title="Carrinho de Compras - ' . SITE_NAME . '">Carrinho de Compras <span>' . $carrinho . '</span></a></li>
';
if($URL[0] == 'curso'):
	$menu = '
	<li><a href="' . BASE . '" title="Início - ' . SITE_NAME . '">Início</a></li>
	<li><a href="#curso" title="Curso - ' . SITE_NAME . '">Curso</a></li>
	<li><a href="#presente" title="Presente - ' . SITE_NAME . '">Presente</a></li>
	<li class="header_logo_menu"><a href="' .  BASE . '" title="Logotipo ' .  SITE_NAME  . '">
		<img width="170" title="Logotipo ' .  SITE_NAME . '" alt="[Logotipo ' .  SITE_NAME . ']" src="' .  INCLUDE_PATH  . '/images/logotipo.png" />	
	</a></li>
	<li><a href="#sobre" title="Sobre - ' . SITE_NAME . '">Sobre</a></li>
	<li><a href="#modulos" title="Módulos - ' . SITE_NAME . '">Módulos</a></li>
	<li><a href="#professor" title="Professor - ' . SITE_NAME . '">Professor</a></li>
	';
endif;
?>
<!--ABRE HEADER-->
<header class="container header_principal" id="menuHeader">
	<h1 class="fontzero"><?= SITE_NAME ?> - <?= SITE_SUBNAME ?></h1>
    <div class="row">
		<a class="header_logo" href="<?= BASE ?>" title="Logotipo <?= SITE_NAME  ?>">
			<img title="Logotipo <?= SITE_NAME ?>" alt="[Logotipo <?= SITE_NAME ?>]" src="<?= INCLUDE_PATH  ?>/images/logotipo.png" />	
		</a>
	    <img class="header_botao_menu_mobile" title="Abrir Menu" alt="[Abrir Menu]" src="<?= INCLUDE_PATH ?>/images/icon_menu_mobile.png" />

		<div class="col">
			<ul class="header_menu_desktop">				
				<?= $menu ?>
			</ul>			
		</div>

		<ul id="menu_mobile" class="header_menu_mobile">
			<img class="header_botao_menu_mobile branco" title="Fechar Menu" alt="[Fechar Menu]" src="<?= INCLUDE_PATH ?>/images/icone-menu-mobile-branco.png" />
			<?= $menu ?>
		</ul>   
	</div>
</header>
<div class="marginteste"></div>
<!--FECHA HEADER-->