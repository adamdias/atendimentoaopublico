<!-- ABRE APRESENTAÇÃO -->
<article class="container apresentacao">
    <div class="row">
        <div class="col">
            <h1>Cursos pensados e produzidos para barbeiros e barbearias</h1>
            <p class="tagline">Barbeiro, aqui estão os cursos que você não sabia que existiam, mas que farão a sua carreira decolar.</p>
            <a href="<?= BASE . '/cursos' ?>" title="Veja nossos cursos!">Veja nossos cursos!</a>
        </div>
    </div>
</article>
<!-- FECHA APRESENTAÇÃO -->

<!-- ABRE CURSOS EM DESTAQUE -->
<?php
$Read->FullRead("SELECT course_id, course_vendor_price, course_comingsoon, course_name, course_title, course_desc, course_cover FROM " . DB_EAD_COURSES . " WHERE course_status = 1 AND course_name != :name ORDER BY course_created DESC LIMIT 6", "name=atendimento-curso-gratuito-1");
if($Read->getResult()):
    $cursos_html = null;
    foreach ($Read->getResult() as $CS):
        extract($CS);
        // pega imagem
        $course_cover = (file_exists("uploads/{$course_cover}") && !is_dir("uploads/{$course_cover}") ? "uploads/{$course_cover}" : 'admin/_img/no_image.jpg');

        // pega texto
        $texto = ($course_vendor_price > 0 ? 'COMPRAR' : "INSCREVER");

        // pega btn para add no carrinho
        require '_cdn/widgets/eadpagseguro/cart.add.php';

        // pega item
        $cursos_html .= '
        <div class="col col-33">
            <div class="zoom">
                <img class="curso_img" alt="[' . $course_title . ']" title="' . $course_title . '" src="' . BASE .'/tim.php?src=' . $course_cover . '&w=' . round(IMAGE_W / 2) . '&h=' . round(IMAGE_H / 2.5) . '"/>
                <article class="curso_item">
                    <h1><a href="' . BASE . '/curso/' . $course_name . '" title="' . $course_title . '">' . $course_title . (!empty($course_comingsoon) ? ' <b style="color: orange">(Em breve)</b>' : '') . '</a></h1>
                    <p class="tagline">' . Check::Chars($course_desc, 80) . '</p>
                    <ul class="curso_links">
                        <li><a href="' . BASE . '/curso/' . $course_name . '" title="' . $course_title . '">Detalhes</a></li>
                        ' . (!empty($course_comingsoon) ? '<li style="margin-right: 36px;"></li>' : '<li>' . $cart . '</li>') . '
                    </ul>
                </article>
            </div>
        </div>
        ';
    endforeach;
    ?>
    <section class="container medium cursos">
        <div class="row">
            <div class="col section_titulo">
                <h1>Cursos em Destaque</h1>
                <p class="tagline">O atendimento de excelência é o segredo para o sucesso!</p>
            </div>
        </div>
        <div class="row">
            <?= $cursos_html ?>
        </div>
    </section>
    <?php
endif;
?>
<!-- FECHA CURSOS EM DESTAQUE -->

<!-- ABRE ISCA DIGITAL -->
<section class="iscadigital">
    <article class="iscadigital_item isca_ebook">
        <img src="<?= INCLUDE_PATH ?>/images/icon_isca_ebook.png" alt="[E-book - 7 sinais de que sua barbearia está no caminho certo]" title="E-book - 7 sinais de que sua barbearia está no caminho certo">
        <h1>Baixe o nosso e-book <a class="iscadigital_free" href="#" title="Baixe aqui este e-book">gratuitamente!</a></h1>
        <p class="tagline">7 sinais de que sua barbearia está no caminho certo</p>
        <a class="iscadigital_btn" href="<?= BASE ?>/ebook-7-sinais" title="Baixe aqui este e-book">Baixe aqui este e-book!</a>
    </article>

    <?php
    // pega o curso gratuito
    $Read->FullRead("SELECT course_id FROM " . DB_EAD_COURSES . " WHERE course_name = :name AND course_status = 1", "name=atendimento-curso-gratuito-1");
    if($Read->getResult()):
        $course_id = $Read->getResult()[0]['course_id'];
        $texto = 'Acesse agora suas aulas';
        // pega botão do curso gratuito
        require '_cdn/widgets/eadpagseguro/cart.add.php';
        ?>
        <article class="iscadigital_item isca_curso">
            <img src="<?= INCLUDE_PATH ?>/images/icon_isca_curso.png" alt="[E-book - Como lidar com cliente irritado]" title="E-book - Como lidar com cliente irritado">
            <h1>Cadastre-se e acesse <a class="iscadigital_free cursofree" href="#" title="Acesse agora suas aulas">gratuitamente!</a></h1>
            <p class="tagline">5 videoaulas dos nossos cursos!</p>
            <a class="iscadigital_btn cursofree" href="#" title="Acesse agora suas aulas">Acesse agora suas aulas!</a>
            <?= $cart ?>
        </article>
        <?php
    endif;
    ?>
</section>
<!-- FECHA QUEM SOMOS -->

<!-- ABRE DIFERENCIAL -->
<section class="container diferencial">
    <div class="content">
        <div class="row">
            <div class="col section_titulo" style="margin-bottom: 0">
                <h1>O que o cliente espera do atendimento?</h1>
                <p class="tagline"></p>
            </div>
            <?php
            for($i=0;$i<4;$i++):
            	if($i == 0):
            		$diferencial['titulo'] = 'Ser chamado sempre pelo o nome';
                    $diferencial['video'] = '301964995';
                    $diferencial['desc'] = 'Conhecer o cliente em função das necessidades é essencial para a qualidade no atendimento.';
            		$diferencial['link'] = 'qualidade-no-atendimento';
            	elseif($i == 1):
            		$diferencial['titulo'] = 'Conversa agradável e sem erros gramaticais';
                    $diferencial['video'] = '301967811';
                    $diferencial['desc'] = 'A correção gramatical é parte essencial da construção da imagem do profissional de excelência.';
            		$diferencial['link'] = 'lingua-portuguesa-para-barbeiros';
            	elseif($i == 2):
            		$diferencial['titulo'] = 'Não ser contaminado por vírus e bactérias';
                    $diferencial['video'] = '306799033';
                    $diferencial['desc'] = 'Vírus, fungos e bactérias representam perigos reais para o cliente e para o profissional de barbeiro.';
                    $diferencial['link'] = 'microbiologia-higiene-total';
            	else:
                    $diferencial['titulo'] = 'Descubra muito mais no nosso canal';
            		$diferencial['img'] = '<img src="' . INCLUDE_PATH . '/images/youtube-call-green.png" alt="[Conheça nosso canal no Youtube]" title="Conheça nosso canal no Youtube">';
            		$diferencial['desc'] = 'Temas 100% focados no Atendimento em Barbearias. Soluções para os desafios diários do trabalho!';
            		$diferencial['out-link'] = 'https://www.youtube.com/channel/UCnF5KKn_XNF1GE3D5SlnNIA?disable_polymer=true';
            	endif;
                ?>
                <div class="col col-25 diferencial_item">
                    <article>
                        <h1><?= $diferencial['titulo'] ?></h1>
                        <div class="embed-container">
                            <?php
                            if(!empty($diferencial['img'])):
                                echo $diferencial['img'];
                            else:
                                if(is_numeric($diferencial['video'])):
                                    echo '<iframe src="https://player.vimeo.com/video/' . $diferencial['video'] . '?title=0&byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                                else:
                                    echo '<iframe src="https://www.youtube.com/embed/' . $diferencial['video'] . '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
                                endif;
                            endif;
                            ?>
                        </div>
                        <p class="tagline"><?= $diferencial['desc'] ?></p>
                        <a <?= (!empty($diferencial['out-link']) ? 'target="_blank"' : '')?> href="<?= (!empty($diferencial['out-link']) ? $diferencial['out-link'] :  BASE . '/curso/' . $diferencial['link']) ?>" title="Ver mais detalhes">Mais detalhes</a>
                    </article>
                </div>
                <?php
            endfor;
            ?>
        </div>
    </div>
</section>
<!-- FECHA DIFERENCIAL -->