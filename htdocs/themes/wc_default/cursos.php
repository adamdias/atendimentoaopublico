<?php
if (!$Read):
    $Read = new Read;
endif;
?>
<!-- ABRE TÍTULO -->
<div class="container big pagina_titulo">
    <div class="content">
        <div class="row">
            <div class="col">
                <h1>Nossos Cursos</h1>
                <p class="tagline">Conheça todos os cursos disponíveis na nossa plataforma</p>
            </div>
        </div>
    </div>
</div>
<!-- FECHA TÍTULO -->

<!-- ABRE CURSOS -->
<section class="container">
    <div class="content">
        <div class="row">
            <?php
            $Page = (!empty($URL[1]) ? $URL[1] : 1);
            $Pager = new Pager(BASE . "/cursos/", "<<", ">>", 5);
            $Pager->ExePager($Page, 9);
            $Read->ExeRead(DB_EAD_COURSES, "WHERE course_status = 1 AND course_name != :name ORDER BY course_title ASC LIMIT :limit OFFSET :offset", "limit={$Pager->getLimit()}&offset={$Pager->getOffset()}&name=atendimento-curso-gratuito-1");
            if (!$Read->getResult()):
                $Pager->ReturnPage();
                Erro("Ainda não existe posts cadastrados. Por favor, volte mais tarde :)", E_USER_NOTICE);
            else:
                $cursos_html = null;
                foreach ($Read->getResult() as $CS):
                    extract($CS);
                    // pega imagem
                    $course_cover = (file_exists("uploads/{$course_cover}") && !is_dir("uploads/{$course_cover}") ? "uploads/{$course_cover}" : 'admin/_img/no_image.jpg');
            
                    // pega texto
                    $texto = ($course_vendor_price > 0 ? 'COMPRAR' : "INSCREVER");

                    // pega btn para add no carrinho
                    require '_cdn/widgets/eadpagseguro/cart.add.php';

                    // pega item
                    $cursos_html .= '
                    <div class="col col-33">
                        <div class="zoom">
                            <img class="curso_img" alt="[' . $course_title . ']" title="' . $course_title . '" src="' . BASE .'/tim.php?src=' . $course_cover . '&w=' . round(IMAGE_W / 2) . '&h=' . round(IMAGE_H / 2.5) . '"/>
                            <article class="curso_item">
                                <h1><a href="' . BASE . '/curso/' . $course_name . '" title="' . $course_title . '">' . $course_title . '</a></h1>
                                <p class="tagline">' . Check::Chars($course_desc, 80) . '</p>
                                <ul class="curso_links">
                                    <li><a href="' . BASE . '/curso/' . $course_name . '" title="' . $course_title . '">Detalhes</a></li>
                                    <li>' . $cart . '</li>
                                </ul>
                            </article>
                        </div>
                    </div>
                    ';
                endforeach;
            
                // pega paginação
                $Pager->ExePaginator(DB_EAD_COURSES, "WHERE course_status = :status", "status=1");
                $paginacao = $Pager->getPaginator();
            endif;
            ?>
            <?= !empty($erro) ? $erro : null ?>
            <?= !empty($cursos_html) ? $cursos_html : null ?>
            <?= !empty($paginacao) ? $paginacao : null ?>
        </div>
    </div>
</section>
<!-- FECHA CURSOS -->