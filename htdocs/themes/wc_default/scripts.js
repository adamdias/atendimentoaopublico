$(function () {
    $('.header_botao_menu_mobile').click(function () {
        $('.header_menu_mobile').fadeToggle(0);
    });

    $('.header_menu_mobile a').click(function(){
        $('.header_menu_mobile').fadeToggle(0);
    });

    $('.cursofree').click(function(){
        $('.isca_curso .wc_cart_add').submit();
        return false;
    });

    var header_principal = $('#menuHeader');
    $(window).scroll(function(){
        if($(window).scrollTop() > 1){
            header_principal.css({
                "position": "fixed",
                "z-index": "99",
                "top": 0
            });
            $('.header_logo_menu').css("width", "100");
            $('.header_logo img').css("width", "100");
            $(".header_botao_menu_mobile").css("top", "28px");
            $(".marginteste").css({
                "display": "block",
                "margin-top": "80px"
            });
        } else {
            $(".header_botao_menu_mobile").css("top", "50px");
            $('.header_logo_menu').css("width", "170");
            $('.header_logo img').css("width", "170");
            header_principal.css({
                "position": "relative",
                "top": 0
            });
            $(".marginteste").css("display", "none");
        }
    });

    //SCROLMOTION
    var headerTop = $('.menuHeader').outerHeight();

    //EFEITO DE DESLIZAR COM OS LINK DO MENU PRINCIPAL
    $('#menuHeader a').click(function () {
        var goto = $("#" + $(this).attr("href").replace("#", "")).position().top;
        $('html, body').animate({scrollTop: goto - (headerTop + 90)}, 1000);

        return false;
    });
});