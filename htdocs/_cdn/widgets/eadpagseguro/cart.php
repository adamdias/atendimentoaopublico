<?php

$CartAction = trim(strip_tags($URL[1]));
$CartBaseUI = BASE . '/campus';

echo '<article class="workcontrol_cart" id="cart">';

$getCart = filter_input(INPUT_GET, "c", FILTER_VALIDATE_INT);
if(!empty($getCart) && $getCart == 12 && !empty($_SESSION['wc_free'])):
    unset($_SESSION['wc_free']);
    header('Location:' . BASE . '/pedido/login');
endif;

//CHECK CART
if (empty($_SESSION['wc_order']) && empty($_SESSION['wc_free']) && $CartAction != 'pagamento' && $CartAction != 'obrigado'):
    echo '
    <div class="container big pagina_titulo">
        <div class="content">
            <div class="row">
                <div class="col">
                    <h1>Carrinho de Compras</h1>
                    <p class="tagline">Escolha seu(s) curso(s) e finalize seu pedido!</p>
                </div>
            </div>
        </div>
    </div>';
    echo "<div class='workcontrol_cart_clean'>";
    echo "<p class='title'><span>&#10008;</span>Oppsss, seu carrinho de compras está vazio! :(</p>";
    echo "<p>Para continuar comprando, navegue pelos cursos que estão disponíveis no nosso site.</p>";
    echo "<a class='btn btn_green' title='Escolher Cursos!' href='" . BASE . "/cursos'>ESCOLHER CURSOS!</a>";
    echo "</div>";
else:
    //CART CLEAR
    if ($CartAction == 'clear'):
        unset($_SESSION['wc_order']);
        header('Location: ' . BASE . '/pedido/home');
        exit;
    endif;

    //CART FRONT-CONTROLER
    if ($CartAction == 'home'):
        //CART HOME
        echo '
        <div class="container big pagina_titulo" style="margin-top: -20px;>
            <div class="content">
                <div class="row">
                    <div class="col">
                        <h1>Carrinho de Compras</h1>
                        <p class="tagline">Escolha seu(s) curso(s) e finalize seu pedido!</p>
                    </div>
                </div>
            </div>
        </div>';
        echo "<div class='workcontrol_cart_list'>";
        echo "<div class='workcontrol_cart_list_header'><p class='item'>-</p><p class='item'>Curso</p><p>Preço</p><p>-</p></div>";

        $CartTotal = 0;
        $wcCartIds = array();
        foreach($_SESSION['wc_order'] as $key => $value):
            $ItemId = $value;
            $Read->ExeRead(DB_EAD_COURSES, "WHERE course_id = :id AND course_status = 1", "id={$ItemId}");
            if ($Read->getResult()):
                extract($Read->getResult()[0]);
                $wcCartIds[] = $course_id;
                $ItemPrice = $course_vendor_price;
                $CartTotal += $ItemPrice;
                echo "<div class='workcontrol_cart_list_item workcontrol_cart_list_item_{$key}'>";
                echo "<p><img title='{$course_title}' alt='{$course_title}' src='" . BASE . "/tim.php?src=uploads/{$course_cover}&w=" . THUMB_W / 5 . "&h=" . THUMB_H / 5 . "'/></p>";
                echo "<p class='item'><a href='" . BASE . "/curso/{$course_name}' title='Ver detalhes de {$course_title}'>{$course_title}</a></p>";
                echo "<p>" . ($course_vendor_price != $ItemPrice ? "<span class='discount'>De R$ <strike>" . number_format($course_vendor_price, '2', ',', '.') . "</strike></span>Por " : '') . "R$ " . number_format($ItemPrice, '2', ',', '.') . "</p>";
                
                echo "<p><span class='wc_cart_remove' id='{$key}'>X</span></p>";
                echo "</div>";
            else:
                unset($_SESSION['wc_order'][$key]);
            endif;
        endforeach;
        echo "</div>";

        $CartCupom = (!empty($_SESSION['wc_cupom']) ? intval($_SESSION['wc_cupom']) : 0);
        $CartPrice = (empty($_SESSION['wc_cupom']) ? $CartTotal : $CartTotal * ((100 - $_SESSION['wc_cupom']) / 100));

        echo "<div class='wc_cart_total_forms'>";
        echo "<div class='wc_cart_total_cupom'>";
        echo "<p>Cupom:</p><input type='text' value='" . (!empty($_SESSION['wc_cupom_code']) ? $_SESSION['wc_cupom_code'] : '') . "' class='wc_cart_cupom_val'/><button class='wc_cart_cupom'>Aplicar</button><img alt='Calculando Desconto!' title='Calculando Desconto!' src='" . BASE . "/_cdn/widgets/ecommerce/load_g.gif'/>";
        echo "</div>";
        
        echo "<div class='wc_cart_total_price'>";
        echo "<p class='wc_cart_total'><b>Sub-total:</b> R$ <span>" . number_format($CartTotal, '2', ',', '.') . "</span></p>";
        echo "<p class='wc_cart_discount'><b>Cupom:</b> <span>{$CartCupom}</span>%</p>";
        echo "<p class='wc_cart_price'><b>Total:</b> R$ <span>" . number_format($CartPrice, '2', ',', '.') . "</span></p>";
        echo "</div>";
        echo "<div class='wc_cart_actions'>";
        echo "<a class='btn btn_blue' href='" . BASE . "' title='Escolher Mais Cursos!'>Escolher Mais Cursos!</a>";
        echo "<a class='btn btn_green' href='" . BASE . "/pedido/login&c=12' title='Fechar Pedido!'>Fechar Pedido!</a>";
        echo "</div>";
    elseif ($CartAction == 'login'):
        //CART LOGIN
        if (!empty($_SESSION['userLogin']) && !empty($_SESSION['userLogin']['user_cell']) && !empty($_SESSION['userLogin']['user_document'])):
            if(!empty($_SESSION['wc_free'])):
                //ORDER ITENS CREATE
                $Read->FullRead("SELECT course_id, course_title, course_vendor_price, course_end_default FROM " . DB_EAD_COURSES . " WHERE course_id = :id", "id={$_SESSION['wc_free']}");
                if($Read->getResult()):
                    extract($Read->getResult()[0]);
                    // cria pedido
                    $Create = new Create;
                    $Update = new Update;
                    $NewOrder = [
                        'user_id' => $_SESSION['userLogin']['user_id'],
                        'order_status' => 1,
                        'order_coupon' => null,
                        'order_price' => 0,
                        'order_payment' => 800,
                        'order_date' => date('Y-m-d H:i:s')
                    ];
                    $Create->ExeCreate(DB_EAD_COURSES_ORDERS, $NewOrder);
                    $OrderCreateId = $Create->getResult();
                    // cria itens do pedido
                    $CartOrdeItens = [
                        'order_id' => $OrderCreateId,
                        'course_id' => $course_id,
                        'item_name' => $course_title,
                        'item_price' => $course_vendor_price
                    ];
                    $Create->ExeCreate(DB_EAD_COURSES_ORDERS_ITEMS, $CartOrdeItens);

                    // pega dados da matricula
                    $Enrollment['user_id'] = $_SESSION['userLogin']['user_id'];
                    $Enrollment['course_id'] = $CartOrdeItens['course_id'];
                    $Enrollment['enrollment_order'] = $Create->getResult();
                    $Enrollment['enrollment_end'] = ($course_end_default == 0 ? null : date("Y-m-d H:i:s", strtotime("+" . $course_end_default . "months")));
    
                    $Read->FullRead("SELECT enrollment_end, enrollment_id FROM " . DB_EAD_ENROLLMENTS . " WHERE user_id=:us AND course_id=:cs", "us={$Enrollment['user_id']}&cs={$Enrollment['course_id']}");
                    if ($Read->getResult()):
                        // DELETE ORDER E ITEMS
                        $Delete = new Delete;
                        $Delete->ExeDelete(DB_EAD_COURSES_ORDERS, "WHERE order_id = :id", "id={$OrderCreateId}");
                        $Delete->ExeDelete(DB_EAD_COURSES_ORDERS_ITEMS, "WHERE order_id = :id", "id={$OrderCreateId}");

                        //UPDATE ENROLLMENTE
                        $UpdateEnrollmentData = [
                            'enrollment_end' => $Enrollment['enrollment_end']
                        ];
                        $Update->ExeUpdate(DB_EAD_ENROLLMENTS, $UpdateEnrollmentData, "WHERE enrollment_id=:id", "id={$Read->getResult()[0]['enrollment_id']}");
                    else:
                        $validade = (!empty($course_end_default) ? 'até ' . date('d/m/Y H\hi', strtotime($Enrollment['enrollment_end'])) : 'Para sempre');
                        $MailBody = "
                            <p style='font-size: 1.4em;'>Olá {$_SESSION['userLogin']['user_name']},</p>
                            <p>Este e-mail é para agradecer por você ter escolhido o nosso curso para auxiliar no seu aprendizado!</p>
                            <p>Você pode ver mais detalhes dessa matrícula <a href='" . BASE . "/campus' title='Acessar minha conta na plataforma!'>acessando sua conta</a> e verificando em seus cursos!</b></p>
                            <p>DADOS DA MATRÍCULA:</p>
                            <p>
                            <b>Curso:</b> {$CartOrdeItens['item_name']}<br>
                            <b>Liberação:</b> " . date('d/m/Y H\hi') . "<br>
                            <b>Validade:</b> " . $validade . "
                            </p>
                            <p>...</p>
                            <p>Se tiver qualquer problema, não deixe de responder este e-mail!</p>
                        ";

                        require 'cart.email.php';
                        $Mensagem = str_replace('#mail_body#', $MailBody, $MailContent);
                        $Email = new Email;
                        $Email->EnviarMontando("Recebemos seu pedido #" . str_pad($OrderCreateId, 7, 0, 0) . "!", $Mensagem, SITE_NAME, MAIL_USER, "{$_SESSION['userLogin']['user_name']} {$_SESSION['userLogin']['user_lastname']}", $_SESSION['userLogin']['user_email']);

                        //CREATE ENROLLMENTE
                        $Enrollment['enrollment_start'] = date('Y-m-d H:i:s');
                        $Create->ExeCreate(DB_EAD_ENROLLMENTS, $Enrollment);
                    endif;
                endif;

                unset($_SESSION['wc_free']);                
                header('Location: ' . BASE . '/campus');
            else:
                header('Location: ' . BASE . '/pedido/endereco');
            endif;
            exit;
        endif;
        echo '<!-- ABRE TÍTULO -->
        <div class="container big pagina_titulo" style="margin-top: -20px;">
                <div class="row">
                    <div class="col">
                        <h1>Dados Pessoais</h1>
                        <p class="tagline">Forneça seus dados para prosseguir com a sua inscrição</p>
                    </div>
                </div>
        </div>
        <!-- FECHA TÍTULO -->';
        
        echo "<div class='workcontrol_order'>";
        echo "<div class='workcontrol_order_forms'>";
        echo "<form autocomplete='off' class='wc_order_login' method='post' action=''>";
        echo "<label><span>E-mail:</span><input class='wc_order_email' type='email' name='user_email' placeholder='Seu E-mail:' required/></label>";
        echo "<div class='label50'><label><span>Nome:</span><input type='text' name='user_name' placeholder='Seu Primeiro Nome:' required/></label></div>";
        echo "<div class='label50'><label><span>Sobrenome:</span><input type='text' name='user_lastname' placeholder='Seu Último Nome:' required/></label></div>";
        echo "<div class='label50'><label><span>Celular:</span><input class='formPhone' type='text' name='user_cell' placeholder='Seu Telefone:' required/></label></div>";
        echo "<div class='label50 labeldocument'><label><span>CPF:</span><input class='formCpf' type='text' name='user_document' placeholder='Seu CPF:' required/></label></div>";
        echo "<label><span>Senha (mínimo de 6 caracteres):</span><input type='password' name='user_password' placeholder='Sua Senha:' required/></label>";
        echo "
        <label class='checkbox'>
            <span class='checkbox__title'>
                Para prosseguir você deve concordar com nossos 
                <a class='checkbox__link' href='". BASE . "/contrato-e-termos-de-uso.pdf' target='_blank' title='Termos de uso e o nosso contrato de serviço'>termos de uso e o nosso contrato de serviço</a>
                .
            </span>
            <input type='checkbox' name='terms' class='checkbox__input'>
            <span class='checkbox__mark'></span>
        </label>
        ";
        echo "<div class='workcontrol_order_forms_actions'>";
        echo "<button class='btn btn_green wc_button_cart'>CONTINUAR!</button>";
        echo "<img alt='Processando Dados!' title='Processando Dados!' src='" . BASE . "/_cdn/widgets/eadpagseguro/load_g.gif'/>";
        echo "</div>";
        echo "</form>";
        echo "</div>";
        require 'cart.sidebar.php';
        echo "<div class='workcontrol_order_back'>";
        echo "<a href='" . BASE . "/pedido/home#cart' title='Voltar a minha lista de compras!'>Voltar!</a>";
        echo "</div>";
        echo "</div>";
    elseif ($CartAction == 'endereco'):
        //CART ADDR
        if (empty($_SESSION['userLogin'])):
            header('Location: ' . BASE . '/pedido/login');
            exit;
        endif;
        echo '<!-- ABRE TÍTULO -->
        <div class="container big pagina_titulo" style="margin-top: -20px;">
                <div class="row">
                    <div class="col">
                        <h1>Endereço</h1>
                        <p class="tagline">Cadastre o seu endereço para prosseguir com a inscrição do curso...</p>
                    </div>
                </div>
        </div>
        <!-- FECHA TÍTULO -->';
        echo "<div class='workcontrol_order'>";
        echo "<div class='workcontrol_order_forms'>";
        echo "<form autocomplete='off' class='wc_order_create' method='post' action=''>";

        $Read->ExeRead(DB_USERS_ADDR, "WHERE user_id = :id AND (addr_zipcode IS NOT NULL OR addr_zipcode != '') ORDER BY addr_key DESC, addr_name ASC", "id={$_SESSION['userLogin']['user_id']}");
        if ($Read->getResult()):
            echo "<div class='workcontrol_order_addrs'>";
            // echo "<p class='workcontrol_order_newaddr'><span class='btn btn_blue wc_addr_form_open'>Cadastrar Novo Endereço!</span></p>";
            foreach ($Read->getResult() as $Addr):
                echo "<label class='worcontrol_useraddr'><input class='wc_order_user_addr' required type='radio' value='{$Addr['addr_id']}' name='wc_order_addr' id='{$Addr['addr_zipcode']}'/><div><p class='title'>{$Addr['addr_name']}: </p><p>{$Addr['addr_street']}, {$Addr['addr_number']}</p><p>B. {$Addr['addr_district']}, {$Addr['addr_city']}/{$Addr['addr_state']}</p><p>{$Addr['addr_zipcode']}</p></div></label>";
            endforeach;
            echo "</div>";

            echo "<div class='workcontrol_order_newaddr_form'>";
            echo "<p class='workcontrol_order_newaddr'><span class='btn btn_yellow wc_addr_form_close'>Selecionar Um Endereço!</span></p>";
        endif;

        echo "<div class='label50'><label><span>Nome:</span><input type='text' name='addr_name' placeholder='Ex: Minha Casa' required/></label></div>";
        echo "<div class='label50'><label><span>CEP:</span><input class='wc_getCep formCep wc_order_zipcode wc_cart_ship_val' type='text' name='addr_zipcode' placeholder='CEP:' required/></label></div>";
        echo "<div class='label50'><label><span>Logradouro:</span><input class='wc_logradouro' type='text' name='addr_street' placeholder='Nome da Rua:' required/></label></div>";
        echo "<div class='label50'><label><span>Número:</span><input type='text' name='addr_number' placeholder='Informe o número:' required/></label></div>";
        echo "<div class='label50'><label><span>Complemento:</span><input class='wc_complemento' type='text' name='addr_complement' placeholder='Ex: Casa B, Ap101'/></label></div>";
        echo "<div class='label50'><label><span>Bairro:</span><input class='wc_bairro' type='text' name='addr_district' placeholder='Bairro:' required/></label></div>";
        echo "<div class='label50'><label><span>Cidade:</span><input class='wc_localidade' type='text' name='addr_city' placeholder='Cidade:' required/></label></div>";
        echo "<div class='label50'><label><span>Estado:</span><input class='wc_uf' type='text' name='addr_state' placeholder='UF do estado:' required/></label></div>";

        if ($Read->getResult()):
            echo "</div>";
        endif;

        echo "<p class='wc_cart_total_shipment_tag'>Selecione o frete:</p>";
        echo "<div class='workcontrol_shipment wc_cart_total_shipment_result'></div>";
        echo "<div class='workcontrol_order_forms_actions'>";
        echo "<button class='btn btn_green wc_button_cart'>CONTINUAR!</button>";
        echo "<img alt='Processando Dados!' title='Processando Dados!' src='" . BASE . "/_cdn/widgets/ecommerce/load_g.gif'/>";
        echo "</div>";
        echo "</form>";
        echo "</div>";
        require 'cart.sidebar.php';
        echo "<div class='workcontrol_order_back'>";
        echo "<a href='" . BASE . "/pedido/home#cart' title='Voltar a minha lista de compras!'>Voltar!</a>";
        echo "</div>";
        echo "</div>";
    elseif ($CartAction == 'pagamento'):
        //CART CLEAR
        unset($_SESSION['wc_order'], $_SESSION['wc_cupom'], $_SESSION['wc_cupom_code'], $_SESSION['wc_order_addr']);

        //CART PAY
        echo '<!-- ABRE TÍTULO -->
        <div class="container big pagina_titulo" style="margin-top: -20px;">
            <div class="row">
                <div class="col">
                    <h1>Pagamento</h1>
                    <p class="tagline">Informe os dados de pagamento e finalize seu pedido...</p>
                </div>
            </div>
        </div>
        <!-- FECHA TÍTULO -->';

        $user_exists = true;
        $OrderId = filter_var(base64_decode($URL[2]), FILTER_VALIDATE_INT);
        $rss = filter_input(INPUT_GET, "rss", FILTER_DEFAULT);
        if(!empty($rss)):
            $rss = base64_decode($rss);            
            $rss = explode("6038920483024328721", $rss);            
            if(!is_array($rss) || count($rss) != 2):
                $rss = false;
            endif;
        endif;

        // verifica se o usuário está logado
        if(empty($_SESSION['userLogin']['user_id']) && $rss):
            $rss[1] =  intval($rss[1]);
            $rss[1] = date('Y-m-d H:i:s', $rss[1]);         
            $Read->ExeRead(DB_EAD_COURSES_ORDERS, "WHERE order_id = :od AND user_id = :id AND order_date = :dt", "od={$OrderId}&id={$rss[0]}&dt={$rss[1]}");
        elseif(!empty($_SESSION['userLogin']['user_id'])):
            $Read->ExeRead(DB_EAD_COURSES_ORDERS, "WHERE order_id = :od AND user_id = :id", "od={$OrderId}&id={$_SESSION['userLogin']['user_id']}");
        else:
            $user_exists = false;
        endif;

        if (!$OrderId || !$user_exists):
            echo "<div class='workcontrol_cart_clean'>";
            echo "<p class='title'><span>&#10008;</span>Oppsss, não foi possível acessar o pedido! :(</p>";
            echo "<p>Desculpe mas o pedido que você está tentando pagar não existe. Por favor, confira o link de pagamento!</p>";
            echo "<a class='btn btn_green' title='Escolher Cursos!' href='" . BASE . "'>ESCOLHER CURSOS!</a>";
            echo "</div>";
        elseif (!$Read->getResult()):
            echo "<div class='workcontrol_cart_clean'>";
            echo "<p class='title'><span>&#10008;</span>Oppsss, pedido indisponível para pagamento! :(</p>";
            echo "<p>Você tentou acessar o pedido <b>#" . str_pad($OrderId, 7, 0, 0) . "</b>. O mesmo não existe ou está indisponível para pagamento!</p>";
            echo "<a class='btn btn_green' title='Escolher Cursos!' href='" . BASE . "'>ESCOLHER CURSOS!</a>";
            echo "</div>";
        else:
            $CartOrder = $Read->getResult()[0];
            extract($CartOrder);
            if ($order_status == 1 || $order_status == 6 || date('Y-m-d H:i:s', strtotime($order_date . "+" . E_ORDER_DAYS . "days")) < date('Y-m-d H:i:s')):
                echo "<div class='workcontrol_cart_clean'>";
                echo "<p class='title'><span>&#10008;</span>O pedido #" . str_pad($order_id, 7, 0, 0) . " não pode ser pago!</p>";
                echo "<p>O status deste pedido é <b>" . getOrderStatus($order_status) . "</b>, pedidos cancelados ou concluídos não podem ser pagos!</p>";
                echo "<a class='btn btn_green' title='Escolher Cursos!' href='" . BASE . "'>Escolha cursos para um novo pedido!</a>";
                echo "</div>";
            else:
                $_SESSION['wc_payorder'] = $CartOrder;
                echo "<div class='workcontrol_order'>";
                echo "<div class='workcontrol_order_forms'>";
                require 'PagSeguroWc/Payment.workcontrol.php';
                echo "</div>";
                require 'cart.order.php';
                echo "</div>";
            endif;
        endif;
    elseif ($CartAction == 'obrigado'):
        if (empty($_SESSION['wc_payorder'])):
            echo '
            <div class="container big pagina_titulo" style="margin-top: -20px;">
                <div class="row">
                    <div class="col">
                        <h1>Detalhes do pedido</h1>
                        <p class="tagline">Confira as informações sobre o seu pedido!</p>
                    </div>
                </div>
            </div>
            ';
            echo "<div class='workcontrol_cart_clean'>";
            echo "<p class='title'><span>&#10008;</span>Oppsss, não foi possível acessar o pedido! :(</p>";
            echo "<p>Desculpe mas o pedido que você está tentando acessar não existe. Por favor, confira o link ou crie um novo pedido!</p>";
            echo "<a class='btn btn_green' title='Escolher Cursos!' href='" . BASE . "'>ESCOLHER CURSOS!</a>";
            echo "</div>";
        else:
            $Read = new Read;
            $Read->ExeRead(DB_EAD_COURSES_ORDERS, "WHERE order_id = :orid", "orid={$_SESSION['wc_payorder']['order_id']}");
            if (!$Read->getResult()):
                echo '
                <div class="container big pagina_titulo" style="margin-top: -20px;">
                    <div class="row">
                        <div class="col">
                            <h1>Detalhes do pedido</h1>
                            <p class="tagline">Confira as informações sobre o seu pedido!</p>
                        </div>
                    </div>
                </div>
                ';
                echo "<div class='workcontrol_cart_clean'>";
                echo "<p class='title'><span>&#10008;</span>Oppsss, não foi possível acessar o pedido! :(</p>";
                echo "<p>Desculpe mas o pedido que você está tentando acessar não existe. Por favor, confira o link ou crie um novo pedido!</p>";
                echo "<a class='btn btn_green' title='Escolher Cursos!' href='" . BASE . "'>ESCOLHER CURSOS!</a>";
                echo "</div>";
            else:
                extract($Read->getResult()[0]);
                $Read->FullRead("SELECT user_name, user_email FROM " . DB_USERS . " WHERE user_id = :oruser", "oruser={$user_id}");
                $UserOrder = $Read->getResult()[0];
                echo '
                <div class="container big pagina_titulo" style="margin-top: -20px;">
                    <div class="row">
                        <div class="col">
                            <h1>&#10003 Pedido Confirmado <span>#' . str_pad($order_id, 7, 0, 0) . '</h1>
                            <p class="tagline">Confira as informações sobre o seu pedido!</p>
                        </div>
                    </div>
                </div>
                ';
                echo "<div class='workcontrol_order'>";

                echo "<div class='trigger trigger_success workcontrol_trigger_order'>";
                echo "<b>Caro(a) {$UserOrder['user_name']},</b>";
                echo "<p>Você recebeu em seu endereço <b>{$UserOrder['user_email']}</b> um e-mail com todos os detalhes do seu pedido. Que foi pago via " . getOrderPayment($order_payment) . " e encontra-se aguardando a confirmação do pagamento!</p>";
                echo "<p>Assim que o pagamento for compensado liberaremos o acesso ao seu(s) curso(s)!</p>";
                echo "</div>";

                echo "<article class='workcontrol_order_completed'>";
                echo "<header>";
                echo "<h1><span>Compra realizada em " . date("d/m/Y H\hi", strtotime($order_date)) . " via " . getOrderPayment($order_payment) . "</span>";
                if ($order_billet):
                    echo "<a class='btn btn_green fl_right' title='Imprimir Boleto' target='_blanck' href='{$order_billet}'>&#x274F; Imprimir Boleto!</a>";
                endif;
                echo "</h1><div class='clear'></div></header>";

                echo "<div class='workcontrol_order_completed_card'><p class='product'>Curso</p><p>Preço</p><p>Total</p></div>";
                $SideTotalCart = 0;
                $SideTotalExtra = 0;
                $SideTotalPrice = 0;
                $Read->ExeRead(DB_EAD_COURSES_ORDERS_ITEMS, "WHERE order_id = :orid", "orid={$order_id}");
                if ($Read->getResult()):
                    $wcCartIds = array();
                    foreach ($Read->getResult() as $SideProduct):
                        $wcCartIds[] = $SideProduct['course_id'];
                        if ($SideProduct['course_id']):
                            echo "<div class='workcontrol_order_completed_card items'>";
                            $Read->FullRead("SELECT course_cover FROM " . DB_EAD_COURSES . " WHERE course_id = :pid", "pid={$SideProduct['course_id']}");
                            echo "<p class='product'><img title='{$SideProduct['item_name']}' alt='{$SideProduct['item_name']}' src='" . BASE . "/tim.php?src=uploads/{$Read->getResult()[0]['course_cover']}&w=" . THUMB_W / 5 . "&h=" . THUMB_H / 5 . "'/><span>" . Check::Chars($SideProduct['item_name'], 42) . "</span></p>";
                            echo "<p>R$ " . number_format($SideProduct['item_price'], '2', ',', '.') . "</p>";
                            echo "<p>R$ " . number_format($SideProduct['item_price'], '2', ',', '.') . "</p>";
                            $SideTotalCart += $SideProduct['item_price'];
                            echo "</div>";
                        else:
                            $SideTotalExtra += $SideProduct['item_price']   ;
                        endif;
                    endforeach;
                endif;

                $TotalCart = $SideTotalCart;
                $TotalExtra = $SideTotalExtra;
                echo "<div class='workcontrol_order_completed_card total'>";
                echo "<div class='wc_cart_total'>Sub-total: <b>R$ <span>" . number_format($TotalCart, '2', ',', '.') . "</span></b></div>";
                if ($order_coupon):
                    echo "<div class='wc_cart_discount'>Desconto: <b><strike>R$ <span>" . number_format($SideTotalCart * ($order_coupon / 100), '2', ',', '.') . "</span></strike></b></div>";
                endif;
                if ($order_installments > 1):
                    echo "<div>Total : <b>R$ <span>" . number_format($order_price, '2', ',', '.') . "</span></b></div>";
                    echo "<div class='wc_cart_price'><small><sup>{$order_installments}x</sup> R$ {$order_installment} : </small><b>R$ <span>" . number_format($order_installments * $order_installment, '2', ',', '.') . "</span></b></div>";
                else:
                    echo "<div class='wc_cart_price'>Total : <b>R$ <span>" . number_format($order_price, '2', ',', '.') . "</span></b></div>";
                endif;
                echo "</div>";
                echo "</article>";

                echo "</div>";
            endif;
        endif;
    else:
        header("Location: " . BASE . "/pedido/home");
        exit;
    endif;
endif;
echo '</article>';
