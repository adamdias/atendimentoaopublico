<?php

session_start();

$getPost = filter_input_array(INPUT_POST, FILTER_DEFAULT);

if (empty($getPost) || empty($getPost['action'])):
    die('Acesso Negado!');
endif;

$strPost = array_map('strip_tags', $getPost);
$POST = array_map('trim', $strPost);

$Action = $POST['action'];
$jSON = null;
unset($POST['action']);

usleep(2000);

if (empty($_SESSION['wc_shipment_zip'])):
    unset($_SESSION['wc_shipment']);
endif;

require '../../../_app/Config.inc.php';
$Read = new Read;
$Create = new Create;
$Update = new Update;

switch ($Action):
    //CART ADD ITEM ON CLICK +
    case 'wc_cart_add':
        if (empty($_SESSION['wc_order'])):
            $_SESSION['wc_order'] = array();
        endif;

        $Read->FullRead("SELECT course_title, course_vendor_price FROM " . DB_EAD_COURSES . " WHERE course_id = :id", "id={$POST['course_id']}");
        
        if (!$Read->getResult()):
            $jSON['trigger'] = AjaxErro("<b>OPPSSS:</b> O curso solicitado não foi encontrado. Por favor, tente novamente!", E_USER_NOTICE);
        elseif($Read->getResult()[0]['course_vendor_price'] == 0):
            $_SESSION['wc_free'] = $POST['course_id'];
            $jSON['redirect'] = BASE . '/pedido/login';
        else:
            if(!in_array($POST['course_id'], $_SESSION['wc_order'])):
                $_SESSION['wc_order'][] = $POST['course_id'];
            endif;
            $jSON['redirect'] = BASE . '/pedido/home#cart';
        endif;
    break;

    //CART REMOVE ON CLICK X
    case 'wc_cart_remove':
        unset($_SESSION['wc_order'][$POST['stock_id']]);

        $CartTotal = 0;
        foreach($_SESSION['wc_order'] as $key => $value):
            $ItemId = $value;
            $Read->ExeRead(DB_EAD_COURSES, "WHERE course_id = :id AND course_status = 1", "id={$ItemId}");
            if ($Read->getResult()):
                extract($Read->getResult()[0]);
                $ItemPrice = $course_vendor_price;
                $CartTotal += $ItemPrice;
            endif;
        endforeach;

        $CartPrice = (empty($_SESSION['wc_cupom']) ? $CartTotal : $CartTotal * ((100 - $_SESSION['wc_cupom']) / 100));
        $jSON['cart_total'] = number_format($CartTotal, '2', ',', '.');
        $jSON['cart_price'] = number_format($CartPrice, '2', ',', '.');
        break;

    //ADD CUPOM
    case 'cart_cupom':
        $Read->FullRead("SELECT cp_id, cp_title, cp_discount, cp_hits FROM " . DB_EAD_COURSES_COUPONS . " WHERE cp_start <= NOW() AND cp_end >= NOW() AND cp_coupon = :cp", "cp={$POST['cupom_id']}");
        if (!$Read->getResult()):
            unset($_SESSION['wc_cupom'], $_SESSION['wc_cupom_code']);
            $jSON['trigger'] = AjaxErro("<b>OPPSSS:</b> Desculpe mas o cupom <b>{$POST['cupom_id']}</b> não existe ou está com sua oferta inativa hoje :(", E_USER_WARNING);
        else:
            $Coupon = $Read->getResult()[0];
            $_SESSION['wc_cupom'] = $Coupon['cp_discount'];
            $_SESSION['wc_cupom_code'] = $POST['cupom_id'];
            $UpdateCupom = ['cp_hits' => $Coupon['cp_hits'] + 1];
            $Update->ExeUpdate(DB_EAD_COURSES_COUPONS, $UpdateCupom, "WHERE cp_id = :cp", "cp={$Coupon['cp_id']}");
            $jSON['trigger'] = AjaxErro("Parabéns, o seu cupom <b>{$Coupon['cp_title']}</b> com <b>{$Coupon['cp_discount']}% de desconto</b> foi aplicado com sucesso :)");
        endif;

        $CartTotal = 0;
        foreach($_SESSION['wc_order'] as $key => $value):
            $ItemId = $value;
            $Read->ExeRead(DB_EAD_COURSES, "WHERE course_id = :id AND course_status = 1", "id={$ItemId}");
            if ($Read->getResult()):
                extract($Read->getResult()[0]);
                $wcCartIds[] = $course_id;
                $ItemPrice = $course_vendor_price;
                $CartTotal += $ItemPrice;
            else:
                unset($_SESSION['wc_order'][$key]);
            endif;
        endforeach;

        $CartPrice = (empty($_SESSION['wc_cupom']) ? $CartTotal : $CartTotal * ((100 - $_SESSION['wc_cupom']) / 100));
        $jSON['cart_cupom'] = (!empty($_SESSION['wc_cupom']) ? $_SESSION['wc_cupom'] : 0);
        $jSON['cart_price'] = number_format($CartPrice, '2', ',', '.');
        break;

    //LOOK USER E-MAIL
    case 'wc_order_email':
        if (empty($POST['user_email'])):
            $jSON['error'] = "<p class='wc_order_error'>&#10008; Informe seu e-mail!</p>";
        elseif (!Check::Email($POST['user_email']) || !filter_var($POST['user_email'])):
            $jSON['error'] = "<p class='wc_order_error'>&#10008; Este não é um e-mail válido!</p>";
        else:
            $Read->FullRead("SELECT user_name, user_lastname, user_document, user_cell FROM " . DB_USERS . " WHERE user_email = :mm", "mm={$POST['user_email']}");
            if ($Read->getResult()):
                $jSON = $Read->getResult()[0];
                $jSON['user'] = true;
            else:
                $jSON['user'] = null;
            endif;
        endif;
        break;

    //USER AUTENTICATION
    case 'wc_order_user':
        $sucesso = false;
        if (in_array('', $POST)):
            $jSON['error'] = "<p class='wc_order_error'>&#10008; Preencha esse campo!</p>";
        elseif (!Check::Email($POST['user_email']) || !filter_var($POST['user_email'], FILTER_VALIDATE_EMAIL)):
            $jSON['field'] = 'user_email';
            $jSON['error'] = "<p class='wc_order_error'>&#10008; Este não é um e-mail válido!</p>";
        elseif (!empty($POST['user_document']) && !Check::CPF($POST['user_document'])):
            $jSON['field'] = 'user_document';
            $jSON['error'] = "<p class='wc_order_error'>&#10008; Este não é um CPF válido!</p>";
        elseif (strlen($POST['user_password']) < 6):
            $jSON['field'] = 'user_password';
            $jSON['error'] = "<p class='wc_order_error'>&#10008; A senha deve ter no míniom 6 caracteres!</p>";
        elseif (empty($POST['terms'])):
            $jSON['field'] = 'terms';
            $jSON['error'] = "<p class='wc_order_error'>&#10008; Você tem que aceitar o termo de adesão e o contrato de serviço para prosseguir!</p>";
        else:
            unset($POST['terms']);
            $Read->FullRead("SELECT user_id FROM " . DB_USERS . " WHERE user_email = :mm", "mm={$POST['user_email']}");
            if (!$Read->getResult()):
                $Read->FullRead("SELECT user_email FROM " . DB_USERS . " WHERE user_document = :dc", "dc={$POST['user_document']}");
                if ($Read->getResult()):
                    $jSON['field'] = 'user_document';
                    $jSON['error'] = "<p class='wc_order_error'>&#10008; CPF já cadastrado em <b>{$Read->getResult()[0]['user_email']}</b>!</p>";
                else:
                    //CREATE NEW USER
                    $UserPassBook = str_repeat("*", strlen($POST['user_password']) - 4) . substr($POST['user_password'], strlen($POST['user_password']) - 4);
                    $POST['user_password'] = hash('sha512', $POST['user_password']);
                    $POST['user_channel'] = 'Novo pedido';
                    $POST['user_registration'] = date('Y-m-d H:i:s');
                    $POST['user_level'] = 1;
                    $POST['user_login_cookie'] = hash("sha512", time());

                    $Create->ExeCreate(DB_USERS, $POST);
                    $POST['user_id'] = $Create->getResult();
                    $_SESSION['userLogin'] = $POST;

                    //SEND CREATE ACCOUNT
                    require_once 'cart.email.php';
                    $BodyMail = "
                        <p style='font-size: 1.3em'>Caro(a) {$POST['user_name']},</p>
                        <p>Este e-mail é para dar a você as boas vindas a nosso site!</p>
                        <p>Uma nova conta foi criada para que você possa ter mais comodidade e agilidade ao interagir conosco. Ao logar-se em sua conta, você pode:</p>
                        <p>
                        ✓ Atualizar seus dados pessoais!<br>
                        ✓ Acompanhar o andamento dos seus pedidos!<br>
                        ✓ Realizar novos pedidos com mais agilidade!<br>
                        ✓ Ter acesso a ofertas exclusivas do site por e-mail!
                        </p>
                        <p>Confira abaixo os dados de acesso a sua conta:</p>
                        <p style='font-size: 1.1em'>
                            Email/Usuário: {$POST['user_email']}<br>
                            Senha: {$UserPassBook}<br>
                        </p>
                        <p><a title='Minha Conta' target='_blank' href='" . BASE . "/campus'>Acessar Minha Conta!</a></p>
                        <p>Ao acessar nosso site, você pode usar esses dados para identificar-se, e assim ter acesso ao melhor do nosso conteúdo...</p>
                        <p><b>Seja muito bem-vindo(a) {$POST['user_name']}.</b></p>
                        <p><i>Atenciosamente, " . SITE_NAME . "!</i></p>
                    ";
                    $Mensagem = str_replace('#mail_body#', $BodyMail, $MailContent);
                    $SendEmail = new Email;
                    $SendEmail->EnviarMontando("Seja bem-vindo(a) {$POST['user_name']}", $Mensagem, SITE_NAME, MAIL_USER, "{$POST['user_name']} {$POST['user_lastname']}", $POST['user_email']);
                    $jSON['redirect'] = BASE . '/pedido/endereco#cart';                    
                    setcookie('wc_ead_login', $POST['user_login_cookie'], time() + 2592000, '/');
                    $sucesso = true;
                endif;
            else:
                //LOGIN USER
                $UserEmail = $POST['user_email'];
                $UserPass = hash("sha512", $POST['user_password']);
                $Read->ExeRead(DB_USERS, "WHERE user_email = :em AND user_password = :ps", "em={$UserEmail}&ps={$UserPass}");
                if ($Read->getResult()):
                    unset($POST['user_email'], $POST['user_password']);
                    $Update->ExeUpdate(DB_USERS, $POST, "WHERE user_id = :id", "id={$Read->getResult()[0]['user_id']}");
                    $_SESSION['userLogin'] = $Read->getResult()[0];
                    $jSON['redirect'] = BASE . '/pedido/endereco#cart';
                    $sucesso = true;
                else:
                    $jSON['field'] = 'user_password';
                    $jSON['error'] = "<p class='wc_order_error'>&#10008; A senha informada não confere! <a title='Recuperar Senha!' href='" . BASE . "/conta/recuperar'>[ Esqueceu sua senha? ]</a></p>";
                endif;
            endif;
        endif;

        // matricula curso gratuito
        if($sucesso && !empty($_SESSION['wc_free'])):
            //ORDER ITENS CREATE
            $Read->FullRead("SELECT course_id, course_title, course_vendor_price, course_end_default FROM " . DB_EAD_COURSES . " WHERE course_id = :id", "id={$_SESSION['wc_free']}");
            if($Read->getResult()):
                extract($Read->getResult()[0]);
                // cadastra pedido
                $NewOrder = [
                    'user_id' => $_SESSION['userLogin']['user_id'],
                    'order_status' => 1,
                    'order_coupon' => null,
                    'order_price' => 0,
                    'order_payment' => 800,
                    'order_date' => date('Y-m-d H:i:s')
                ];
                $Create->ExeCreate(DB_EAD_COURSES_ORDERS, $NewOrder);
                $OrderCreateId = $Create->getResult();
                // pega item do pedido
                $CartOrdeItens = [
                    'order_id' => $OrderCreateId,
                    'course_id' => $course_id,
                    'item_name' => $course_title,
                    'item_price' => $course_vendor_price
                ];
                $Create->ExeCreate(DB_EAD_COURSES_ORDERS_ITEMS, $CartOrdeItens);

                // pega dados da matricula
                $Enrollment['user_id'] = $_SESSION['userLogin']['user_id'];
                $Enrollment['course_id'] = $CartOrdeItens['course_id'];
                $Enrollment['enrollment_order'] = $Create->getResult();
                $Enrollment['enrollment_end'] = ($course_end_default == 0 ? null : date("Y-m-d H:i:s", strtotime("+" . $course_end_default . "months")));

                // verifica se a matricula já existe
                $Read->FullRead("SELECT enrollment_end, enrollment_id FROM " . DB_EAD_ENROLLMENTS . " WHERE user_id=:us AND course_id=:cs", "us={$Enrollment['user_id']}&cs={$Enrollment['course_id']}");
                if ($Read->getResult()):
                    // DELETE ORDER E ITEMS
                    $Delete = new Delete;
                    $Delete->ExeDelete(DB_EAD_COURSES_ORDERS, "WHERE order_id = :id", "id={$OrderCreateId}");
                    $Delete->ExeDelete(DB_EAD_COURSES_ORDERS_ITEMS, "WHERE order_id = :id", "id={$OrderCreateId}");

                    //UPDATE ENROLLMENTE
                    $UpdateEnrollmentData = [
                        'enrollment_end' => $Enrollment['enrollment_end']
                    ];
                    $Update->ExeUpdate(DB_EAD_ENROLLMENTS, $UpdateEnrollmentData, "WHERE enrollment_id=:id", "id={$Read->getResult()[0]['enrollment_id']}");
                else:
                    $validade = (!empty($course_end_default) ? 'até ' . date('d/m/Y H\hi', strtotime($Enrollment['enrollment_end'])) : 'Para sempre');
                    $MailBody = "
                        <p style='font-size: 1.4em;'>Olá {$_SESSION['userLogin']['user_name']},</p>
                        <p>Este e-mail é para agradecer por você ter escolhido o nosso curso para auxiliar no seu aprendizado!</p>
                        <p>Você pode ver mais detalhes dessa matrícula <a href='" . BASE . "/campus' title='Acessar minha conta na plataforma!'>acessando sua conta</a> e verificando em seus cursos!</b></p>
                        <p>DADOS DA MATRÍCULA:</p>
                        <p>
                        <b>Curso:</b> {$CartOrdeItens['item_name']}<br>
                        <b>Liberação:</b> " . date('d/m/Y H\hi') . "<br>
                        <b>Validade:</b> " . $validade . "
                        </p>
                        <p>...</p>
                        <p>Se tiver qualquer problema, não deixe de responder este e-mail!</p>
                    ";

                    require 'cart.email.php';
                    $Mensagem = str_replace('#mail_body#', $MailBody, $MailContent);
                    $Email = new Email;
                    $Email->EnviarMontando("Recebemos seu pedido #" . str_pad($OrderCreateId, 7, 0, 0) . "!", $Mensagem, SITE_NAME, MAIL_USER, "{$_SESSION['userLogin']['user_name']} {$_SESSION['userLogin']['user_lastname']}", $_SESSION['userLogin']['user_email']);

                    //CREATE ENROLLMENTE
                    $Enrollment['enrollment_start'] = date('Y-m-d H:i:s');
                    $Create->ExeCreate(DB_EAD_ENROLLMENTS, $Enrollment);
                endif;
                unset($_SESSION['wc_free']);
                $jSON['redirect'] = BASE . "/campus";
            endif;
        endif;
        break;

    //WORK CONTROL ORDER CREATE
    case 'wc_order_create':
        //ERROR KEY
        $CartError = null;
        if (empty($_SESSION['userLogin'])):
            $jSON['trigger'] = AjaxErro("<b>Erro:</b> Desculpe! Mas não foi possível obter seus dados pessoais para o pedido!<p><b>Atualize a página para tentar novamente!</b></p>", E_USER_ERROR);
            break;
        endif;

        //NEW ADDR
        if (!empty($POST['addr_name'])):
            $UpdateAddr = ['addr_key' => null];
            $Update->ExeUpdate(DB_USERS_ADDR, $UpdateAddr, "WHERE user_id = :id", "id={$_SESSION['userLogin']['user_id']}");

            $AddrCheck = $POST;
            unset($AddrCheck['addr_complement']);
            if (in_array('', $AddrCheck)):
                $jSON['form_error'] = "<p class='wc_order_error'>&#10008; Preencha esse campo!</p>";
                $CartError = true;
            else:
                $NewAddr = [
                    'user_id' => $_SESSION['userLogin']['user_id'],
                    'addr_key' => 1,
                    'addr_name' => $POST['addr_name'],
                    'addr_zipcode' => $POST['addr_zipcode'],
                    'addr_street' => $POST['addr_street'],
                    'addr_number' => $POST['addr_number'],
                    'addr_complement' => (!empty($POST['addr_complement']) ? $POST['addr_complement'] : null),
                    'addr_district' => $POST['addr_district'],
                    'addr_city' => $POST['addr_city'],
                    'addr_state' => $POST['addr_state'],
                    'addr_country' => SITE_ADDR_COUNTRY
                ];
                $Read->FullRead("SELECT addr_id, addr_zipcode FROM " . DB_USERS_ADDR . " WHERE user_id = :uid ", "uid={$_SESSION['userLogin']['user_id']}");
                if(!$Read->getResult()):
                    $Create->ExeCreate(DB_USERS_ADDR, $NewAddr);
                    $_SESSION['wc_order_addr'] = $Create->getResult();
                elseif(!empty($Read->getResult()[0]['addr_zipcode'])):
                    $_SESSION['wc_order_addr'] = $Read->getResult()[0]['addr_id'];
                else:
                    $Delete = new Delete;
                    $Delete->ExeDelete(DB_USERS_ADDR, "WHERE user_id = :id", "id={$_SESSION['userLogin']['user_id']}");
                    $Create->ExeCreate(DB_USERS_ADDR, $NewAddr);
                    $_SESSION['wc_order_addr'] = $Create->getResult();
                endif;
            endif;
        endif;

        if(!empty($POST['wc_order_addr'])):
            $_SESSION['wc_order_addr'] = $POST['wc_order_addr'];
        endif;

        //ADDR CHECK
        if (empty($_SESSION['wc_order_addr'])):
            $jSON['trigger'] = AjaxErro("<b class='icon-info'>ENDEREÇO:</b> É preciso cadastrar ou selecionar um endereço para finalizar seu pedido!</p>", E_USER_NOTICE);
            break;
        endif;

        if (!$CartError):
            //ORDER MOUNT
            $CartTotal = 0;
            foreach ($_SESSION['wc_order'] as $key => $value):
                $Read->FullRead("SELECT course_title, course_id, course_vendor_price FROM " . DB_EAD_COURSES . " WHERE course_id = :id", "id={$value}");
                if ($Read->getResult()):
                    extract($Read->getResult()[0]);
                    $CartTotal += $course_vendor_price;
                    $CartOrdeItens[] = [
                        'course_id' => $course_id,
                        'item_name' => $course_title,
                        'item_price' => $course_vendor_price
                    ];
                endif;
            endforeach;
            $CartPrice = (empty($_SESSION['wc_cupom']) ? $CartTotal : $CartTotal * ((100 - $_SESSION['wc_cupom']) / 100));
            $CartTotalPrice = $CartPrice;

            //ORDER CREATE
            $NewOrder = [
                'user_id' => $_SESSION['userLogin']['user_id'],
                'order_status' => 3,
                'order_coupon' => (!empty($_SESSION['wc_cupom']) ? $_SESSION['wc_cupom'] : null),
                'order_price' => $CartTotalPrice,
                'order_payment' => 1,
                'order_addr' => $_SESSION['wc_order_addr'],
                'order_date' => date('Y-m-d H:i:s')
            ];
            $Create->ExeCreate(DB_EAD_COURSES_ORDERS, $NewOrder);
            $OrderCreateId = $Create->getResult();

            //ORDER ITENS CREATE
            foreach ($CartOrdeItens as $Key => $Value):
                $CartOrdeItens[$Key]['order_id'] = $OrderCreateId;
                $Create->ExeCreate(DB_EAD_COURSES_ORDERS_ITEMS, $CartOrdeItens[$Key]);
            endforeach;

            //SEND MAIL :: ORDER CREATED
            $BodyMail = "<p style='font-size: 1.2em;'>Caro(a) {$_SESSION['userLogin']['user_name']},</p>";
            $BodyMail .= "<p>Obrigado pela preferência. informamos que seu pedido #" . str_pad($OrderCreateId, 7, 0, 0) . " foi registrado com sucesso em nosso site.</p>";
            $BodyMail .= "<p>Neste momento estamos apenas esperando a confirmação do pagamento para liberá-lo a você...</p>";
            $BodyMail .= "<p>Ainda não pagou? <a href='" . BASE . "/pedido/pagamento/" . base64_encode($OrderCreateId) . "&rss=" . base64_encode($NewOrder['user_id'] . '6038920483024328721' . strtotime($NewOrder['order_date'])). "' title=''>PAGAR AGORA!</a></p></p>";
            $BodyMail .= "<p style='font-size: 1.4em;'>Confira os detalhes do seu pedido:</p>";
            $BodyMail .= "<p>Pedido: " . str_pad($OrderCreateId, 7, 0, STR_PAD_LEFT) . "<br>Data: " . date('d/m/Y H\hi', strtotime($NewOrder['order_date'])) . "<br>Valor: R$ " . number_format($NewOrder['order_price'], '2', ',', '.') . "</p>";
            $BodyMail .= "<hr><table style='width: 100%'><tr><td>STATUS:</td><td style='color: #00AD8E; text-align: center;'>✓ Aguardando Pagamento</td><td style='color: #888888;  text-align: center;'>✓ Processando</td><td style='color: #888888; text-align: right;'>✓ Concluído</td></tr></table><hr>";
            $Read->ExeRead(DB_EAD_COURSES_ORDERS_ITEMS, "WHERE order_id = :order", "order={$OrderCreateId}");
            if ($Read->getResult()):
                $i = 0;
                $ItemsPrice = 0;
                $BodyMail .= "<p style='font-size: 1.4em;'>Cursos:</p>";
                $BodyMail .= "<p>Abaixo você pode conferir os detalhes e valores de cada curso adquirido em seu pedido. Confira:</p>";
                $BodyMail .= "<table style='width: 100%' border='0' cellspacing='0' cellpadding='0'>";
                foreach ($Read->getResult() as $Item):
                    $i++;
                    $ItemsPrice += $Item['item_price'];
                    $BodyMail .= "<tr><td style='border-bottom: 1px solid #cccccc; padding: 10px 0 10px 0;'>" . str_pad($i, 5, 0, STR_PAD_LEFT) . " - " . Check::Words($Item['item_name'], 5) . "</td><td style='border-bottom: 1px solid #cccccc; padding: 10px 0 10px 0; text-align: right;'>R$ " . number_format($Item['item_price'], '2', ',', '.') . "</td><td style='border-bottom: 1px solid #cccccc; padding: 10px 0 10px 0; text-align: right;'>R$ " . number_format($Item['item_price'], '2', ',', '.') . "</td></tr>";
                endforeach;
                if (!empty($NewOrder['order_coupon'])):
                    $BodyMail .= "<tr><td style='border-bottom: 1px solid #cccccc; padding: 10px 0 10px 0;'>Cupom:</td><td style='border-bottom: 1px solid #cccccc; padding: 10px 0 10px 0; text-align: right;'>{$NewOrder['order_coupon']}% de desconto</td><td style='border-bottom: 1px solid #cccccc; padding: 10px 0 10px 0; text-align: right;'>- <strike>R$ " . number_format($ItemsPrice * ($NewOrder['order_coupon'] / 100), '2', ',', '.') . "</strike></td></tr>";
                endif;
                $BodyMail .= "<tr style='background: #cccccc;'><td style='border-bottom: 1px solid #cccccc; padding: 10px 10px 10px 10px;'>{$i} curso(s) no pedido</td><td style='border-bottom: 1px solid #cccccc; padding: 10px 10px 10px 10px; text-align: right;'>Itens</td><td style='border-bottom: 1px solid #cccccc; padding: 10px 10px 10px 10px; text-align: right;'>R$ " . number_format($NewOrder['order_price'], '2', ',', '.') . "</td></tr>";
                $BodyMail .= "</table>";
            endif;
            $BodyMail .= "<p>Qualquer dúvida não deixe de entrar em contato {$_SESSION['userLogin']['user_name']}. Obrigado por sua preferência mais uma vez...</p>";
            $BodyMail .= "<p><i>Atenciosamente " . SITE_NAME . "!</i></p>";

            require 'cart.email.php';
            $Mensagem = str_replace('#mail_body#', $BodyMail, $MailContent);
            $Email = new Email;
            $Email->EnviarMontando("Recebemos seu pedido #" . str_pad($OrderCreateId, 7, 0, 0) . "!", $Mensagem, SITE_NAME, MAIL_USER, "{$_SESSION['userLogin']['user_name']} {$_SESSION['userLogin']['user_lastname']}", $_SESSION['userLogin']['user_email']);

            //PAYMENT REDIRECT
            $jSON['redirect'] = BASE . "/pedido/pagamento/" . base64_encode($OrderCreateId) . "#cart";
        endif;
endswitch;

echo json_encode($jSON);