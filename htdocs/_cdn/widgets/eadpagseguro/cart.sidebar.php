<?php
if(!empty($_SESSION['wc_free'])):
?>
<section class='workcontrol_order_details'>
    <h1><span class="noborder">Cadastre-se e ganhe o curso a seguir:</span></h1>
    <?php
    $Read->ExeRead(DB_EAD_COURSES, "WHERE course_id = :id AND course_status = 1", "id={$_SESSION['wc_free']}");
    if ($Read->getResult()[0]):
        $SideProduct = $Read->getResult()[0];
        echo "<p>";
        echo "<img title='{$SideProduct['course_title']}' alt='{$SideProduct['course_title']}' src='" . BASE . "/tim.php?src=uploads/{$SideProduct['course_cover']}&w=" . THUMB_W / 10 . "&h=" . THUMB_H / 10 . "'/>";
        echo "<span>" . Check::Chars($SideProduct['course_title'], 42) . "</span>";
        echo "</p>";
    endif;
    ?>
    <div class="workcontrol_order_details_total">
        <div class="wc_cart_price"><b>Gratuito</b></div>
    </div>
</section>
<?php
elseif (!empty($_SESSION['wc_order'])):
    $OderDetail = $_SESSION['wc_order'];
    $OderCupom = (!empty($_SESSION['wc_cupom']) ? $_SESSION['wc_cupom'] : null);
    ?><section class='workcontrol_order_details'>
        <h1><span>Resumo do pedido:</span></h1>
        <?php
        $SideTotalCart = 0;
        foreach ($OderDetail as $key => $value):
            $Read->ExeRead(DB_EAD_COURSES, "WHERE course_id = :id AND course_status = 1", "id={$value}");
            if ($Read->getResult()[0]):
                $SideProduct = $Read->getResult()[0];
                $SideProductPrice = $SideProduct['course_vendor_price'];

                echo "<p>";
                echo "<img title='{$SideProduct['course_title']}' alt='{$SideProduct['course_title']}' src='" . BASE . "/tim.php?src=uploads/{$SideProduct['course_cover']}&w=" . THUMB_W / 10 . "&h=" . THUMB_H / 10 . "'/>";
                echo "<span>" . Check::Chars($SideProduct['course_title'], 42) . "<br>R$ " . number_format($SideProductPrice, '2', ',', '.') . "</span>";
                echo "</p>";
                $SideTotalCart += $SideProductPrice;
            endif;
        endforeach;

        $SideTotalPrice = (!empty($_SESSION['wc_cupom']) ? $SideTotalCart * ((100 - $_SESSION['wc_cupom']) / 100) : $SideTotalCart);
        ?>
        <div class="workcontrol_order_details_total">
            <?php if ($OderCupom): ?>
                <div class="wc_cart_discount">Desconto: <b><strike>R$ <span><?= number_format($SideTotalCart * ($OderCupom / 100), '2', ',', '.'); ?></span></strike></b></div>
            <?php endif; ?>
            <div class="wc_cart_price">Total: <b>R$ <span><?= number_format($SideTotalPrice, '2', ',', '.'); ?></span></b></div>
        </div>
    </section><?php
endif;