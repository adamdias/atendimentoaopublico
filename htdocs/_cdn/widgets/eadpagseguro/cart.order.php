<section class='workcontrol_order_details'>
    <h1 style="margin: 0 0 20px 0;"><span>&#10003 Pedido #<?= str_pad($order_id, 7, 0, 0); ?></span></h1>
    <article style="margin: 0 0 20px 0;">
        <?php
        $Read = new Read;
        $Read->FullRead("SELECT user_name, user_lastname FROM " . DB_USERS . " WHERE user_id = :oruser", "oruser={$user_id}");
        $User = $Read->getResult()[0];
        ?>
        <h1 class="row">Por <?= "{$User['user_name']} {$User['user_lastname']} dia " . date("d/m/Y \a\s H\hi", strtotime($order_date)); ?></h1>
    </article>
    <article>
        <h1 class="title">Itens do Pedido:</h1>
        <?php
        $SideTotalCart = 0;
        $SideTotalExtra = 0;
        $SideTotalPrice = 0;
        $Read->ExeRead(DB_EAD_COURSES_ORDERS_ITEMS, "WHERE order_id = :orid", "orid={$order_id}");
        if ($Read->getResult()):
            foreach($Read->getResult() as $ITEMS):
                $Read->ExeRead(DB_EAD_COURSES, "WHERE course_id = :id", "id={$ITEMS['course_id']}");
                if($Read->getResult()):
                    extract($Read->getResult()[0]);
                    echo "<p>";
                    echo "<img title='{$course_title}' alt='{$course_title}' src='" . BASE . "/tim.php?src=uploads/{$course_cover}&w=" . THUMB_W / 10 . "&h=" . THUMB_H / 10 . "'/>";
                    echo "<span>" . Check::Chars($course_title, 42) . "<br>R$ " . number_format($course_vendor_price, '2', ',', '.') . "</span>";
                    echo "</p>";
                    $SideTotalCart += $ITEMS['item_price'];    
                endif;
            endforeach;
        endif;

        $TotalCart = $SideTotalCart;
        ?>
        <div class="workcontrol_order_details_total">
            <div class="wc_cart_total">Sub-total: <b>R$ <span><?= number_format($TotalCart, '2', ',', '.'); ?></span></b></div>
            <?php if ($order_coupon): ?>
                <div class="wc_cart_discount">Desconto: <b><strike>R$ <span><?= number_format($SideTotalCart * ($order_coupon / 100), '2', ',', '.'); ?></span></strike></b></div>
            <?php endif; ?>
            <div class="wc_cart_price">Total : <b>R$ <span><?= number_format($order_price, '2', ',', '.'); ?></span></b></div>
        </div>
    </article>
</section>