$(function () {
    var action = $('link[rel="base"]').attr('href') + "/_cdn/widgets/eadpagseguro/cart.ajax.php";

    //ADD ITEM TO CART
    $('.wc_cart_add').submit(function () {
        var data = $(this).serialize() + "&action=wc_cart_add";
        $.post(action, data, function (data) {
            if (data.trigger) {
                wcCartTrigger(data.trigger);
            }
            if(data.redirect) {
                window.location = data.redirect;
            }
        }, 'json');
        return false;
    });

    //CART REMOVE
    $('.wc_cart_remove').click(function () {
        var stock_id = $(this).attr('id');
        $.post(action, {action: 'wc_cart_remove', stock_id: stock_id}, function (data) {
            $('.workcontrol_cart_list_item_' + stock_id).fadeOut(200, function () {
                $(this).remove();
                if (!$('.workcontrol_cart_list_item').length) {
                    window.location.reload();
                }
            });
            $('.wc_cart_price span').html(data.cart_price);
            $('.wc_cart_total span').html(data.cart_total);
        }, 'json');
    });

    //CART UPDATE CHANGE
    $('.wc_cart_change').focusout(function () {
        var InputChange = $(this);
        InputChange.val(parseInt($(this).val()) - 1);
        $('.wc_cart_change_plus[id="' + InputChange.attr('id') + '"]').click();
    });

    //CUPOM CALC
    $('.wc_cart_cupom').click(function () {
        var input = $('.wc_cart_cupom_val');
        if (!input.val()) {
            wcCartTrigger("<div class='trigger trigger_info'><b>OPPSSS:</b> Informe o código do cupom para aplicar e aproveitar seu desconto especial!</div>");
        } else {
            $('.wc_cart_total_cupom img').fadeIn();
            $.post(action, {action: 'cart_cupom', cupom_id: input.val()}, function (data) {
                $('.wc_cart_total_cupom img').fadeOut();
                $('.wc_cart_discount span').html(data.cart_cupom);
                $('.wc_cart_price span').html(data.cart_price);

                if (data.trigger) {
                    wcCartTrigger(data.trigger);
                }
            }, 'json');
        }
    });

    //USER DATA CAPTURE
    $('.wc_order_email').change(function () {
        var Form = $('.wc_order_login');
        var Input = $(this);

        Form.find('img').fadeIn(400);
        $('.wc_order_error').remove();

        $.post(action, {action: 'wc_order_email', user_email: Input.val()}, function (data) {
            Form.find('img').fadeOut(400);
            if (data.user) {
                Form.find('input[name="user_name"]').val(data.user_name);
                Form.find('input[name="user_lastname"]').val(data.user_lastname);
                Form.find('input[name="user_cell"]').val(data.user_cell);
                if (data.user_document) {
                    Form.find('.labeldocument input').attr('disabled', true);
                    Form.find('.labeldocument').fadeOut(200);
                } else {
                    Form.find('.labeldocument input').attr('disabled', false);
                    Form.find('.labeldocument').fadeIn(200);
                }
            } else {
                Form.find('.labeldocument input').attr('disabled', false);
                Form.find('.labeldocument').fadeIn(200);
            }

            setTimeout(function () {
                Form.find('.btn').click();
            }, 200);

            if (data.error) {
                Input.after(data.error);
                $('.wc_order_error').fadeIn();
            }
        }, 'json');
    });

    //USER FORM SEND
    $('.wc_order_login').submit(function () {
        var Form = $(this);
        var Data = Form.serialize() + "&action=wc_order_user";

        Form.find('img').fadeIn(400);
        $('.wc_order_error').remove();
        $('.wc_order_email_false').val(Form.find('input[name="user_email"]').val());

        $.post(action, Data, function (data) {
            Form.find('img').fadeOut(400);
            if (data.error) {
                if (data.field) {
                    Form.find("input[name='" + data.field + "']").after(data.error);
                } else {
                    var Inputs = Form.find('input');
                    Inputs.each(function (index, elem) {
                        if (!elem.value) {
                            $(this).after(data.error);
                        }
                    });
                }
                $('.wc_order_error').fadeIn();
            }
            if (data.success) {
                Form.find('input').remove();
            }
            
            if(data.redirect) {
                window.location.href = data.redirect;
            }
        }, 'json');
        return false;
    });

    //USER ADDR SET
    $('.wc_order_user_addr').click(function () {
        $('.wc_cart_manager').fadeIn();
        var AddrInput = $(this);
        $('.workcontrol_order_newaddr_form').find('input').val('').removeAttr('checked').attr('disabled', true);
        $.post(action, {action: 'wc_addr_select', addr_id: AddrInput.val()});
    });

    //USER ADDR GET
    $('.wc_order_zipcode').change(function () {
        var Form = $('.wc_order_create');
        setTimeout(function () {
            Form.find('button').click();
        }, 200);
    });

    //SLIDE FORMS
    $('.wc_addr_form_open').click(function () {
        $('.wc_order_create').find('input[type="text"]').val('');
        $('.wc_order_create').find('input').removeAttr('checked').removeAttr('disabled');
        $('.workcontrol_order_addrs').slideUp(400, function () {
            $('.workcontrol_order_addrs').find('input[type="text"]').val('');
            $('.workcontrol_order_addrs').find('input').removeAttr('checked').attr('disabled', true);
            $('.workcontrol_order_newaddr_form').slideDown(400);
        });
    });

    $('.wc_addr_form_close').click(function () {
        $('.wc_order_create').find('input[type="text"]').val('');
        $('.wc_order_create').find('input').removeAttr('checked').removeAttr('disabled');
        $('.workcontrol_order_newaddr_form').slideUp(400, function () {
            $('.workcontrol_order_newaddr_form').find('input[type="text"]').val('');
            $('.workcontrol_order_newaddr_form').find('input').removeAttr('checked').attr('disabled', true);
            $('.workcontrol_order_addrs').slideDown(400);
        });
    });

    //ORDER CREATE
    $('.wc_order_create').submit(function () {
        var Form = $(this);
        var Data = Form.serialize() + "&action=wc_order_create";
        Form.find('img').fadeIn();

        $.post(action, Data, function (data) {
            if (data.form_error) {
                if (data.field) {
                    Form.find("input[name='" + data.field + "']").after(data.form_error);
                } else {
                    var Inputs = Form.find('input[type="text"][required]');
                    Inputs.each(function (index, elem) {
                        if (!elem.value) {
                            $(this).after(data.form_error);
                        }
                    });
                }
                $('.wc_order_error').fadeIn();
            }

            if (data.redirect) {
                window.location.href = data.redirect;
            } else {
                Form.find('img').fadeOut();
            }

            if (data.trigger) {
                wcCartTrigger(data.trigger);
            }

        }, 'json');
        return false;
    });

    //TRIGGERS ALERT
    function wcCartTrigger(Message) {
        if (Message) {
            $('.wc_cart_callback').html('').fadeOut(1).css('right', '-400px');

            $('.wc_cart_callback').html(Message).fadeIn(400);
            $('.wc_cart_callback').animate({'right': '0'}, 100);

            $('.wc_cart_callback').click(function () {
                $(this).fadeOut(400, function () {
                    $(this).html('').css('right', '-400px');
                });
            });
        } else {
            $('.wc_cart_callback').fadeOut(1, function () {
                $(this).html('').css('right', '-400px');
            });
        }
    }
});