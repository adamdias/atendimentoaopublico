<?php
header("Location: dashboard.php?wc=teach/orders");
die;
$AdminLevel = LEVEL_WC_EAD_ORDERS;
if (!APP_EAD || empty($DashboardLogin) || empty($Admin) || $Admin['user_level'] < $AdminLevel):
    die('<div style="text-align: center; margin: 5% 0; color: #C54550; font-size: 1.6em; font-weight: 400; background: #fff; float: left; width: 100%; padding: 30px 0;"><b>ACESSO NEGADO:</b> Você não esta logado<br>ou não tem permissão para acessar essa página!</div>');
endif;

// AUTO INSTANCE OBJECT READ
if (empty($Read)):
    $Read = new Read;
endif;

$OrderClause = filter_input(INPUT_GET, 's', FILTER_DEFAULT);
$OrdersEadStatus = filter_input(INPUT_GET, 'status', FILTER_DEFAULT);
$WhereCourse = null;
if ($OrderClause):
    $WhereCourse = "WHERE (order_id = '{$OrderClause}')";
elseif (!empty($OrdersEadStatus)):
    $WhereCourse = "WHERE order_status = '{$OrdersEadStatus}'";
endif;

$Search = filter_input_array(INPUT_POST);
if ($Search && ($Search['s'] || isset($Search['status']))):
    $S = urlencode($Search['s']);
    $Status = (!empty($Search['status']) ? $Search['status'] : '');
    header("Location: dashboard.php?wc=teach/orders&status={$Status}&s={$S}");
    exit;
endif;
?>

<header class="dashboard_header">
    <div class="dashboard_header_title">
        <h1 class="icon-cart">Vendas</h1>
        <p class="dashboard_header_breadcrumbs">
            &raquo; <?= ADMIN_NAME; ?>
            <span class="crumb">/</span>
            <a title="<?= ADMIN_NAME; ?>" href="dashboard.php?wc=home">Dashboard</a>
            <span class="crumb">/</span>
            <?= ($OrdersEadStatus ? "Vendas com status " . getOrderStatus($OrdersEadStatus) : 'Vendas'); ?>
        </p>
    </div>

    <div class="dashboard_header_title">
        <h1 class="icon-coin-dollar">Vendas</h1>
        <p class="dashboard_header_breadcrumbs">
            &raquo; <?= ADMIN_NAME; ?>
            <span class="crumb">/</span>
            <a title="<?= ADMIN_NAME; ?>" href="dashboard.php?wc=home">Dashboard</a>
            <span class="crumb">/</span>
            <?= ($OrdersEadStatus ? "Vendas com status " . getWcHotmartStatus($OrdersEadStatus) : 'Vendas'); ?>
        </p>
    </div>

    <div class = "dashboard_header_search">
        <form style="width: 80%; display: inline-block" name="searchOrders" action="" method="post" enctype="multipart/form-data" class="ajax_off">
            <input type="text" name="s" placeholder="ID:" style="width: 25%; margin-right: 3px;"/>
            <select name="status" style="width: 55%; margin-right: 3px; padding: 5px 10px">
                <option value="">Todos</option>
                <?php
                foreach (getOrderStatus() as $Key => $Status):
                    echo "<option " . ($OrdersEadStatus == $Key ? "selected='selected'" : '') . " value='{$Key}'>{$Status}</option>";
                endforeach;
                ?>
            </select>
            <button class="btn btn_blue icon icon-search icon-notext"></button>
        </form>
        <a title = "Recarregar Pedidos" href = "dashboard.php?wc=teach/orders&status=<?= $OrdersEadStatus; ?>" class = "btn btn_green icon-spinner11 icon-notext"></a>
    </div>
</header>
<div class="dashboard_content">
    <section class="box_content">
        <?php
        $getPage = (filter_input(INPUT_GET, 'page'));
        $Page = ($getPage ? $getPage : 1);
        $Pager = new Pager("dashboard.php?wc=teach/orders&status={$OrdersEadStatus}&page=", "<", ">", 3);
        $Pager->ExePager($Page, 15);

        $Read->ExeRead(DB_EAD_COURSES_ORDERS, "{$WhereCourse} ORDER BY order_date DESC LIMIT :limit OFFSET :offset", "limit={$Pager->getLimit()}&offset={$Pager->getOffset()}");
        if (!$Read->getResult()):
            $Pager->ReturnPage();
            echo "<div class='trigger trigger_info trigger_none icon-info al_center font_medium'>Ainda não existem pedidos!</div><div class='clear'></div>";
        else:
            echo "<div class='student_gerent_orders_detail'>
                <div class='student_gerent_orders_detail_content'>
                    <div class='j_order_detail'></div>
                    <p class='close'><span class='icon icon-cross icon-notext btn btn_red order_close j_student_order_close student_gerent_orders_detail_content_close'></span></p>
                </div></div>";

            foreach ($Read->getResult() as $EadOrder):
                extract($EadOrder);

                $Read->LinkResult(DB_USERS, "user_id", $user_id, 'user_id, user_name, user_lastname, user_email');
                $OrderUser = $Read->getResult()[0];

                // pega cursos
                $cursos = null;
                $Read->ExeRead(DB_EAD_COURSES_ORDERS_ITEMS, "WHERE order_id = :oid", "oid={$order_id}");
                if($Read->getResult()):
                    foreach($Read->getResult() as $ITEMS):
                        $Read->FullRead("SELECT course_title FROM " . DB_EAD_COURSES . " WHERE course_id = :course", "course={$ITEMS['course_id']}");
                        $CourseTitle = ($Read->getResult() ? "Curso - {$Read->getResult()[0]['course_title']}<br>" : "");
                        $cursos .= $CourseTitle;
                    endforeach;
                endif;
                ?>
                <article class="wc_ead_orders_order">
                    <h1 class="row icon-cart">
                        #<?= str_pad($order_id, 5, 0, 0); ?>
                        <span>(<?= getOrderPayment($order_payment); ?>)</span>
                    </h1><p class="row icon-user-plus">
                        <a title="Gerenciar Aluno" href="dashboard.php?wc=teach/students_gerent&id=<?= $OrderUser['user_id']; ?>" class="a"><?= "{$OrderUser['user_name']} {$OrderUser['user_lastname']}"; ?></a>
                        <span><?= $OrderUser['user_email']; ?></span>
                    </p><p class="row row_pay icon-calendar">
                        <?= date('d/m/Y H\hi', strtotime($order_date)); ?>
                        <span><?= (substr($cursos, 0, -4)) ?></span>
                    </p><p class="row row_pay">
                        R$ <?= number_format($order_price, '2', ',', '.'); ?>
                    </p><p class="row">
                        <span style="cursor: default;font-weight: bold; width: 100%;" class="btn btn_<?= ($order_status == 1 ? 'green icon-bell' :($order_status == 2 ? 'red icon-cross' : 'yellow icon-spinner9')) ?>" id="<?= $order_id; ?>"><?= getOrderStatus($order_status); ?>
                        </span>
                    </p>
                </article>
                <?php
            endforeach;

            $Pager->ExePaginator(DB_EAD_COURSES_ORDERS, "{$WhereCourse}");
            echo $Pager->getPaginator();
            echo "<div class='clear'></div>";
        endif;
        ?>
    </section>
</div>